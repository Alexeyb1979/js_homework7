let peopleList = [
    {name: 'Anna',
     surname: 'Ivanova',
     gender: 'female',
     age: 25,
    },
    {name: 'Taras',
    surname: 'Shevtsov',
    gender: 'male',
    age: 24,
   },
   {name: 'Yana',
     surname: 'Zabolotko',
     gender: 'female',
     age: 27,
    },
    {name: 'Igor',
     surname: 'Kazakov',
     gender: 'male',
     age: 42,
    }
];

let excludedList = [
    {name: 'Anna',
    surname: 'Ivanova',
    gender: 'female',
    age: 25,
   },
   {name: 'Irina',
   surname: 'Petrenko',
   gender: 'female',
   age: 23,
  },
  {name: 'Taras',
    surname: 'Shevtsov',
    gender: 'male',
    age: 24,
   },
   {name: 'Iurii',
    surname: 'Kravchenko',
    gender: 'male',
    age: 27,
   },
];

// let excludeBy = function (arr, arr1) { //объявляем функцию
//     let resArr = [];
//     arr.forEach(function(item){// перебераем элементы массива peopleList через  forEach
//         let isPerson = true; //создаем переменную isPerson которая по умолчанию true
//         arr1.forEach(function(item2){//на каждом шагу перебора peopleList перебераем элементы массива excludedList
//             if(item.name === item2.name){// и сравниваем значение каждого елемента name в массиве isPerson с name в массиве excludedList   
//                 isPerson = false;//если они совпадают то isPerson ставовится false
//             }
//         });
//         if (isPerson) resArr.push(item);//ecли элементы на  совпадают то ложим их в массив noRepeatList
//     });
//     return resArr;
// }
// console.log(excludeBy(peopleList, excludedList));

// let excludeBy = function (arr, arr1) { //аналогично через filter
//     let resArr = [];
//     arr.filter(function (item){
//         let isPerson = true;
//         arr1.forEach (function(item2){
//             if (item.name === item2.name){
//                 isPerson = false;
//             }
//         });
//         if (isPerson) resArr.push(item);
//     })
//     return resArr;
// }
// console.log(excludeBy(peopleList, excludedList));

let excludeBy = function (arr, arr1) {
    let resArr = arr.filter(item => { //используем filter
        return arr1.every(item2 => item.name != item2.name) // с помощью every проверяем на отсутствие совпадающих 
    })                                                      // значений (item.name != item2.name), и если условие
    return resArr;                                          // соблюдено помещаем елемент в resArr;
};

console.log(excludeBy(peopleList, excludedList));


